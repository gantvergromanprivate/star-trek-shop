import React from 'react';
import { Route } from 'react-router-dom';
import PropTypes from 'prop-types';

import DefaultLayout from '../../Layouts/DefaultLayout';

const appRoute = ({ Layout, ...rest }) => <Layout><Route {...rest} /></Layout>;

appRoute.propTypes = {
  Layout: PropTypes.func,
};

appRoute.defaultProps = {
  Layout: DefaultLayout,
};

export default appRoute;
