import React from 'react';
import { Switch, Redirect } from 'react-router-dom';

import AppRoute from './AppRoute';

import CrewCasting from '../CrewCasting';
import MemberList from '../MemberList';

const Routes = () => (
  <Switch>
    <AppRoute exact path="/" render={() => <Redirect to="/crew" />} />
    <AppRoute exact path="/crew" component={CrewCasting} />
    <AppRoute exact path="/members" component={MemberList} />
  </Switch>
);

export default Routes;
