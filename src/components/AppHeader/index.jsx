import React from 'react';
import { connect } from 'react-redux';

import { NavLink } from 'react-router-dom';
import {
  AppBar,
  Toolbar,
  IconButton,
  Badge,
} from '@material-ui/core';
import { People, Person, Menu } from '@material-ui/icons';
import { makeStyles } from '@material-ui/styles';

import memberPropTypes from '../../schemas/propTypes/member';

const useStyles = makeStyles({
  link: {
    padding: '0.5rem',
    align: 'left',
  },
  activeLink: {
    backgroundColor: '#efefef',
  },
});

const genNavLink = (route, icon, classes) => (
  <NavLink
    to={`/${route}`}
    className={classes.link}
    activeClassName={classes.activeLink}
  >
    {icon}
  </NavLink>
);

const appHeader = ({ memberCount }) => {
  const classes = useStyles();

  const crewNavLink = genNavLink('crew', <Person />, classes);
  let memberNavLink = genNavLink('members', <People />, classes);
  if (memberCount) {
    memberNavLink = <Badge badgeContent={memberCount}>{memberNavLink}</Badge>;
  }

  return (
    <AppBar position="static">
      <Toolbar>
        <IconButton aria-label="Open drawer">
          <Menu />
        </IconButton>
        {crewNavLink}
        {memberNavLink}
      </Toolbar>
    </AppBar>
  );
};

appHeader.propTypes = {
  memberCount: memberPropTypes.memberCount.isRequired,
};

const mapStateToProps = state => ({
  memberCount: state.crew.members.length,
});

export default connect(
  mapStateToProps,
  null,
)(appHeader);
