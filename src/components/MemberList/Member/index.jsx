import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Avatar, Chip } from '@material-ui/core';

import memberPropTypes from '../../../schemas/propTypes/member';
import { removeMember } from '../../../services/crew/actions';

const memberView = ({ member, removeMemberAPI }) => {
  const {
    id,
    name,
    avatar,
  } = member;
  return (
    <Chip
      key={id}
      avatar={<Avatar alt={name} src={avatar} />}
      label={name}
      onDelete={() => removeMemberAPI(id)}
    />
  );
};

memberView.propTypes = {
  member: memberPropTypes.member.isRequired,
  removeMemberAPI: PropTypes.func.isRequired,
};

export default connect(
  null,
  { removeMemberAPI: removeMember },
)(memberView);
