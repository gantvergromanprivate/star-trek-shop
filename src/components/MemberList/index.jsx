import React from 'react';
import { connect } from 'react-redux';

import membersPropTypes from '../../schemas/propTypes/member';

import Member from './Member';

const memberList = (props) => {
  const {
    members,
  } = props;

  return members.map(member => <Member member={member} />);
};

memberList.propTypes = {
  members: membersPropTypes.members.isRequired,
};

const mapStateToProps = state => ({
  members: state.crew.members,
});

export default connect(
  mapStateToProps,
)(memberList);
