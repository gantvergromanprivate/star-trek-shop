import React from 'react';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
  root: {
    position: 'fixed',
    bottom: 0,
    right: 0,
    left: 0,
    padding: '0.5rem',
    backgroundColor: '#efefef',
    textAlign: 'center',
  },
});

const footer = () => {
  const classes = useStyles();
  return <div className={classes.root}>Gantverg Roman (2019)</div>;
};

export default footer;
