import React from 'react';

const spinner = () => (
  <div>Data are loading...</div>
);

export default spinner;
