
import React from 'react';
import PropTypes from 'prop-types';

import AppHeader from '../../AppHeader';
import AppFooter from '../../AppFooter';

const defaultLayout = ({ children }) => (
  <div style={{ height: '100%' }}>
    <header>
      <AppHeader />
    </header>
    <main>
      {children}
    </main>
    <footer>
      <AppFooter />
    </footer>
  </div>
);

defaultLayout.propTypes = {
  children: PropTypes.node,
};

defaultLayout.defaultProps = {
  children: null,
};

export default defaultLayout;
