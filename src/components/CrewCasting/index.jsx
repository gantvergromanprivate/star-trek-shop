import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import characterPropTypes from '../../schemas/propTypes/character';

import fetchCharacters from '../../services/shelf/actions';

import Spinner from '../Spinner';
import SearchCharacter from './SearchCharacter';
import CharacterList from './CharacterList';

const crewCasting = ({
  characters,
  fetchCharactersAPI,
  filters,
  sort,
}) => {
  const [isLoading, setIsLoading] = useState(false);

  const handleFetchCharacters = (
    newFilters = filters,
    newSort = sort,
  ) => {
    setIsLoading(true);
    fetchCharactersAPI(newFilters, newSort, () => setIsLoading(false));
  };

  // useEffect(() => {
  //   handleFetchCharacters();
  // }, []);

  useEffect(() => {
    handleFetchCharacters(filters, sort);
  }, [filters, sort]);

  return (
    <React.Fragment>
      {isLoading && <Spinner />}
      <SearchCharacter />
      <CharacterList characters={characters} />
    </React.Fragment>
  );
};

crewCasting.propTypes = {
  fetchCharactersAPI: PropTypes.func.isRequired,
  characters: characterPropTypes.characters.isRequired,
  filters: PropTypes.string,
  sort: PropTypes.string,
};

crewCasting.defaultProps = {
  filters: '',
  sort: '',
};

const mapStateToProps = state => ({
  characters: state.shelf.characters,
  filters: state.filters.items,
  sort: state.sort.type,
});

export default connect(
  mapStateToProps,
  { fetchCharactersAPI: fetchCharacters },
)(crewCasting);
