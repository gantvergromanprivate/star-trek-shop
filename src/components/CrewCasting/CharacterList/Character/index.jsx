import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';

import {
  Card,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  Collapse,
  IconButton,
  Typography,
} from '@material-ui/core';
import {
  ExpandMore as ExpandMoreIcon,
  Share as ShareIcon,
  PersonAdd as AddIcon,
  Done as DoneIcon,
} from '@material-ui/icons';

import { makeStyles } from '@material-ui/styles';

import characterPropTypes from '../../../../schemas/propTypes/character';
import { addMember } from '../../../../services/crew/actions';

const useStyles = makeStyles({
  card: {
    maxWidth: 400,
    margin: '20px',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    // transition: theme.transitions.create('transform', {
    //   duration: theme.transitions.duration.shortest,
    // }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
});

const characterView = ({
  character,
  employed,
  addMemberAPI,
}) => {
  const {
    id,
    name,
    fullName,
    img,
    biografy,
  } = character;
  const [expanded, switchExpanded] = useState(false);
  const classes = useStyles();

  return (
    <Card className={classes.card} key={id}>
      <CardHeader
        title={name}
      />
      <CardMedia
        className={classes.media}
        image={img}
        title={fullName}
      />
      <CardActions className={classes.actions} disableActionSpacing>
        <IconButton
          aria-label="Add to members"
          onClick={() => addMemberAPI(character)}
          disabled={employed}
        >
          { employed ? <DoneIcon /> : <AddIcon /> }
        </IconButton>
        <IconButton aria-label="Share">
          <ShareIcon />
        </IconButton>
        <IconButton
          className={classnames(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={() => switchExpanded(!expanded)}
          aria-expanded={expanded}
          aria-label="Show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>
            {biografy}
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
};

characterView.propTypes = {
  character: characterPropTypes.character.isRequired,
  employed: PropTypes.bool,
  addMemberAPI: PropTypes.func.isRequired,
};

characterView.defaultProps = {
  employed: true,
};

export default connect(
  null,
  { addMemberAPI: addMember },
)(characterView);
