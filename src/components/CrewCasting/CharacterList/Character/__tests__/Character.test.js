import React from 'react';
import { mount } from 'enzyme';

import Character from '..';
import Root from '../../../../Root';

const characterMock = {
  id: 42,
  name: 'Kirk, James T.',
  fullName: 'James Tiberius Kirk',
  race: 'human',
  urls: [
    'https://en.wikipedia.org/wiki/James_T._Kirk',
    'http://www.startrek.com/database_article/james-t-kirk',
  ],
};

it('mount without crashing', () => {
  const wrapped = mount(
    <Root>
      <Character character={characterMock} addMember={() => {}} />
    </Root>
  );
  wrapped.unmount();
});
