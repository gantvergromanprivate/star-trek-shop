import React from 'react';
import { connect } from 'react-redux';

import characterPropTypes from '../../../schemas/propTypes/character';
import memberPropTypes from '../../../schemas/propTypes/member';

import Character from './Character';

const characterList = ({ characters, members }) => characters.map((character) => {
  const isEmployed = members.some(({ id }) => character.id === id);
  return (<Character character={character} key={character.id} employed={isEmployed} />);
});

characterList.propType = {
  characters: characterPropTypes.characters.isRequired,
  members: memberPropTypes.members.isRequired,
};

const mapStateToProps = state => ({
  members: state.crew.members,
});

export default connect(
  mapStateToProps,
  null,
)(characterList);
