import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { makeStyles } from '@material-ui/styles';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';

import updateFilters from '../../../services/filters/actions';

const useStyles = makeStyles({
  root: {
    position: 'relative',
    top: '0.5rem',
    right: 0,
    left: 0,
    padding: '0.5rem',
    backgroundColor: '#efefef',
    textAlign: 'left',
  },
});

const searchCharacter = ({ updateFiltersAPI }) => {
  const filterChangeHandler = ({ target }) => {
    const { value } = target;
    updateFiltersAPI(value);
  };
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <SearchIcon />
      <InputBase
        placeholder="Search…"
        onChange={filterChangeHandler}
      />
    </div>
  );
};

searchCharacter.propTypes = {
  updateFiltersAPI: PropTypes.func.isRequired,
};

export default connect(
  null,
  { updateFiltersAPI: updateFilters },
)(searchCharacter);
