import React from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import PropTypes from 'prop-types';

import Routes from './components/Routes';

import store from './services/store';

const history = createBrowserHistory();

const Root = ({ initialState }) => (
  <Provider store={store(initialState)}>
    <Router history={history}>
      <Routes />
    </Router>
  </Provider>
);

Root.propTypes = {
  initialState: PropTypes.shape({}),
};

Root.defaultProps = {
  initialState: {},
};

export default Root;
