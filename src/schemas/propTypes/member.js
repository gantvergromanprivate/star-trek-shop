import PropTypes from 'prop-types';

const membererShape = PropTypes.shape({
  id: PropTypes.number.isRequired,
  name: PropTypes.string,
  avatar: PropTypes.string,
});

export default {
  member: membererShape,
  members: PropTypes.arrayOf(membererShape),
  memberCount: PropTypes.number,
};
