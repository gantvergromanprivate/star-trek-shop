import PropTypes from 'prop-types';

const characterShape = PropTypes.shape({
  id: PropTypes.number.isRequired,
  name: PropTypes.string,
  fullName: PropTypes.string,
  img: PropTypes.string,
  biografy: PropTypes.string,
});

export default {
  character: characterShape,
  characters: PropTypes.arrayOf(characterShape),
};
