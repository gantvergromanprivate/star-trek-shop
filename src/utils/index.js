import shatnerImage from '../static/images/characters/KirkJamesT.jpg';
import nimoyImage from '../static/images/characters/371px-Leonard_Nimoy_as_Spock_1967.jpg';
import picardImage from '../static/images/characters/638px-Captain_Picard_Chair.jpg';
import worfImage from '../static/images/characters/WorfTNG.jpg';

import shatnerAvatar from '../static/images/avatars/375px-Star_Trek_William_Shatner.jpeg';
import nimoyAvatar from '../static/images/avatars/Spock.jpg';
import picardAvatar from '../static/images/avatars/PicardJeanLuc.jpg';
import worfAvatar from '../static/images/avatars/Worf.jpg';

const characterAPI = () => (Promise.resolve({
  data: {
    characters: [
      {
        id: 42,
        name: 'Kirk, James T.',
        fullName: 'James Tiberius Kirk',
        race: 'human',
        biografy: `James Tiberius Kirk was born in Riverside, Iowa, in the year 2233,[1] where he was raised by his parents, George and Winona Kirk.[2] Although born on Earth, Kirk lived for a time on Tarsus IV, where he was one of nine surviving witnesses to the massacre of 4,000 colonists by Kodos the Executioner. James Kirk's brother, George Samuel Kirk, is first mentioned in "What Are Little Girls Made Of?" and introduced and killed in "Operation: Annihilate!", leaving behind three children.[3]

        Kirk became the first and only student at Starfleet Academy to defeat the Kobayashi Maru test, garnering a commendation for original thinking for reprogramming the computer to make the "no-win scenario" winnable. Kirk was granted a field commission as an ensign and posted to advanced training aboard the USS Republic. He was then promoted to lieutenant junior grade and returned to Starfleet Academy as a student instructor.[3] Students could either "think or sink" in his class, and Kirk himself was "a stack of books with legs".[4] Upon graduating in the top five percent, Kirk was promoted to lieutenant and served aboard the USS Farragut.[3] While assigned to the Farragut, Kirk commanded his first planetary survey and survived a deadly attack that killed a large portion of the Farragut's crew,[3] including his commanding officer, Captain Garrovick. He received his first command, a spaceship roughly equivalent to a destroyer, while still quite young.[5]
        
        Kirk became Starfleet's youngest starship captain after receiving command of the USS Enterprise for a five-year mission,[3] three years of which are depicted in the original Star Trek series.[6] Kirk's most significant relationships in the television series are with first officer Spock and chief medical officer Dr. Leonard "Bones" McCoy.[7] McCoy is someone to whom Kirk unburdens himself and is a foil to Spock.[8] Robert Jewett and John Shelton Lawrence's The Myth of the American Superhero describes Kirk as "a hard-driving leader who pushes himself and his crew beyond human limits".[9] Terry J. Erdman and Paula M. Block, in their Star Trek 101 primer, note that while "cunning, courageous and confident", Kirk also has a "tendency to ignore Starfleet regulations when he feels the end justifies the means"; he is "the quintessential officer, a man among men and a hero for the ages".[10] Although Kirk throughout the series becomes romantically involved with various women, when confronted with a choice between a woman and the Enterprise, "his ship always won".[11] Roddenberry wrote in a production memo that Kirk is not afraid of being fallible, but rather is afraid of the consequences to his ship and crew should he make an error in judgment.[12] Roddenberry wrote:
        
        [Kirk] has any normal man's insecurities and doubts, but he knows he cannot ever show them—except occasionally in private with ship's surgeon McCoy or in subsequent moments with Mr. Spock whose opinions Kirk has learned to value so highly.[12]
        
        In Star Trek: The Motion Picture, Admiral Kirk is Chief of Starfleet Operations, and he takes command of the Enterprise from Captain Willard Decker.[3] Star Trek creator Gene Roddenberry's novelization of The Motion Picture depicts Kirk married to a Starfleet officer killed during a transporter accident.[13][14] At the beginning of Star Trek II: The Wrath of Khan, Kirk takes command of the Enterprise from Captain Spock to pursue his enemy from "Space Seed", Khan Noonien Singh. The movie introduces Kirk's son, David Marcus. Spock, who notes that "commanding a starship is [Kirk's] first, best destiny", dies at the end of Star Trek II. In Star Trek III: The Search for Spock, Admiral Kirk leads his surviving officers in a successful mission to rescue Spock from a planet on which he is reborn. Although Kirk is demoted to Captain in Star Trek IV: The Voyage Home for disobeying Starfleet orders, he also receives command of a new Enterprise, the USS Enterprise-A (NCC 1701-A).[3] The ship is ordered decommissioned at the end of Star Trek VI: The Undiscovered Country.
        
        In Star Trek Generations, Captain Jean-Luc Picard finds Kirk living in the timeless Nexus, despite the fact that history recorded his death during the Enterprise-B's maiden voyage, Kirk having fallen into the Nexus in the incident that caused his "death". Picard convinces Kirk to return to Picard's present to help stop the villain Soran from destroying Veridian III's sun. Although Kirk initially refuses the offer, he agrees after realizing the Nexus cannot give him the one thing he has always sought: the ability to make a difference. The two leave the Nexus and stop Soran. However, Kirk is mortally wounded; as he dies, Picard assures him that he helped to "make a difference". Picard buries Kirk on the planet.`,
        urls: [
          'https://en.wikipedia.org/wiki/James_T._Kirk',
          'http://www.startrek.com/database_article/james-t-kirk',
        ],
        img: shatnerImage,
        avatar: shatnerAvatar,
      },
      {
        id: 43,
        name: 'Spock',
        fullName: 'Spock',
        race: 'HAlf-Vulcan, Half-Human',
        biografy: `Spock's mother, Amanda Grayson, was a human schoolteacher from Earth and his father, Sarek, was a respected diplomat. For most of his life, Spock was torn between his emotional human side and the stern discipline of his Vulcan half until his experience with the V'Ger machine-entity in 2271 and his later death and rebirth in 2286 broadened his perspective. As of 2267 he had earned the Vulcanian Scientific Legion of Honor, had been twice decorated by Starfleet Command and held an A7 computer expert classification.

        As a child, Spock had a pet sehlat—a cuddly Vulcan bear-like animal with claws and fangs. His older half-brother, Sybok, who was ostracized from Vulcan because he rejected the way of pure logic, was killed in 2292 after battling an alien entity at the galaxy's center that claimed to be 'God.'
        
        At age 7, Spock was telepathically bonded with a young Vulcan girl named T'Pring. The telepathic touch would draw the two together when the time was right after both came of age: once every seven years all Vulcan males experiences pon farr, a powerful Vulcan mating drive which demands that they mate or die. In 2267, however, T'Pring chose Stonn, a Vulcan, over Spock, and the Vulcan returned to the U.S.S. Enterprise unwed. He did eventually marry in a ceremony attended by Lt. Jean-Luc Picard.
        
        Because the young Vulcan chose to join Starfleet, he and Sarek opened an 18-year rift over Sarek's hope his son would attend the Vulcan Science Academy. Spock was the first Vulcan to enlist in the Federation Starfleet, serving aboard the U.S.S. Enterprise under Captain Christopher Pike as a lieutenant, and later for James T. Kirk.
        
        After the conclusion of the U.S.S. Enterprise's five-year mission, Spock retired from Starfleet and returned to Vulcan to pursue the emotion-purging of the kohlinar discipline from the Vulcan Masters. Although he completed the training, Spock failed to achieve kohlinar because his emotions were stirred by the V'Ger entity in 2271. He then reentered Starfleet and was eventually promoted to U.S.S. Enterprise captain when that ship was assigned as a training vessel at Starfleet Academy.
        
        Spock sacrificed himself in 2285 to repair plasma conduits that allowed the U.S.S. Enterprise and its crew to escape from the detonation of the Genesis Device by Khan Noonien Singh; his radiation-wracked body was consigned to space but landed on the newly formed Genesis Planet and began regeneration. Prior to his death, Spock had mind-melded with McCoy to transfer his katra, apparently intending for his longtime friend and sparring partner to return it to Vulcan and perhaps be fully regenerated in the fal-tor-pan or refusion process, conducted for the first time in generations.`,
        urls: [
          'https://en.wikipedia.org/wiki/Spock',
          'http://www.startrek.com/database_article/spock',
        ],
        img: nimoyImage,
        avatar: nimoyAvatar,
      },
      {
        id: 44,
        name: 'Jean-Luc Picard',
        fullName: 'Jean-Luc Picard',
        race: 'human',
        biografy: `An accomplished diplomat and tactician, Picard managed to surpass a 22-year career as first officer and later captain of the U.S.S. Stargazer with an even more impressive record as captain of the fleet's former flagship U.S.S. Enterprise. In the latter role he not only witnessed the major turning points of recent galactic history but played a major role in them as well, from surviving as the only human abductee of the Borg invasion in 2366, to becoming the chief contact point with the Q Continuum, to serving as arbiter choosing the current ruler of the Klingon Empire and exposing the Romulans as backers of his chief rivals, later helping a pacifist underground movement to gain a toehold there.

        Owing to a single-minded drive since childhood for a Starfleet career, Picard has "never been a family man" and was long uncomfortable with the Galaxy-class starship's civilian family contingent; the orphaned son of Lt. Marla Aster again raised this concern, although his unease with children has dissipated since being stranded with three youths during a shipboard quantum filament crisis. His initial reaction to family is also reflected in the friction with his father and, later, his older brother over leaving the family business, a winery. However, when asked about having children of his own Picard once replied that "wishing for a thing does not make it so." The issue of lineage and his lack of offspring caused a sustained yet brief period of depression upon the sudden accidental deaths in 2371 of Robert and his nephew Rene, his only other family members. His outlook was also affected by the chance to experience a traditional family through an encounter in the Nexus in 2371, as recounted later, and after having relived 40 years of a Kataanan native's life three years earlier; in the latter case the decades of experience compressed into 30 minutes from a Kataanan archival probe was overwhelming.
        
        Lingering throughout Picard's life is a series of unsuccessful romantic relationships, stemming in part from his introspective nature as a career officer and his self-professed desire to avoid long-term commitments. Significant adult romances have included Jenice Manheim in 2342, Capt. Phillipa Louvois in 2356, rogue archeologist Vash in 2366-68, andLt. Cmdr. Nella Darren in 2369. In addition, he also had barely acknowledged feelings forEns. Marta Batanides following their Starfleet graduation; the Kriosian metamorph Kamala; and the widow of his best friend Lt. Cmdr. Jack Crusher, Beverly — a Starfleet doctor, longtime friend and his chief medical officer on the Enterprise.
        
        Aside from these feelings regarding children, family and women, Picard was even aloof with those he considered his close friends. Nevertheless, he has shown a willingness to stake his career for them — as when defending the inherent sentient's rights of first Data and then his daughter Lal against Starfleet confiscation, then acting as Worf's cha'dich before the Klingon High Council and stepping in on behalf of Crewman Simon Tarses during Adm. Satie's virtual witchhunt. Also, a Q-induced encounter in 2370 with a possible future timeline seems to have diffused this separation from friends somewhat. While he has had no more encounters with his best Academy mates, both of Picard's closest friends from his early career, Jack Crusher and Walker Keel, were killed in the line of duty.
        
        Part of Picard's private nature includes a difficulty in confronting deep personal issues, which then tend to become suppressed. Philosophically, he sees life and death as more than two choices of eternal or momentary existence; in fact, he believes there is another concept yet beyond human understanding. Genetic engineering with its pre-determination disturbs him, saying it robs humanity of the unknown factor that makes life worth living. Having to be patient in the presence of mounting problems, even if it is unavoidable or even the best path to take, is unsettling to him; nevertheless, he has shown a clear skill in knowing when to solicit opinions and when to act decisively. His Enterprise operations officer, Data, once estimated only a 17% chance that Picard would be so indecisive in a crisis as to call Starfleet for instructions.
        
        Though he often heatedly defends a strict interpretation of the Prime Directive, he has broken it numerous times when he felt it was warranted. For example, during his Enterprisecareer he allowed an Edo female to confront her "god" from space and brought a pre-spaceflight Mintakan leader aboard so as to undo the damage done by cultural contamination. (He later offered his life to a distressed Mintakan's arrow to prove he was no immortal himself.) He also chafes at the Starfleet directive banning captains from most away-team missions in uncertain or hostile situations.
        
        Picard had few friends as a youngster and self-admittedly "skipped his childhood," due to his early, single-minded drive to be in Starfleet. Though shy, he took piano lessons only to please his mother; he hated public performance and soon quit — a move he now regrets. He did build airships in bottles when young, and like his nephew years later he wrote a ribbon-winning report on starships; reading of the ancient Bajorans in the fifth grade might have been another influence on his lifelong passion for archeology. Later he was school president, valedictorian and a star athlete.
        
        Picard failed his first try on the Academy entrance exams but only required one more to pass. As a student athlete, he became the only freshman ever to win the Academy marathon — the event at Danula II marked the beginning of his friendship with Admiral Hanson — and he once out-wrestled a Ligonian in 14 seconds with a reverse body lift for a pin. He graduated at or near the top of his Academy class, even though he once failed an Academy class over a woman he refers to only as "A.F.," the initials he carved into gardenerBoothby's prize campus elm tree; he was called at least once to the Academy superintendent's office, and he credits Boothby for helping him through an ordeal that saved his graduation. His lack of self-discipline as a young officer nearly led to his death in a near-fatal stabbing at Starbase Earhart in 2328 while awaiting post-graduation assignments. Picard went on as a lieutenant to meet Ambassador Sarek at the wedding of his son Spock.
        
        Another mentor of those years was archeology professor Richard Galen, whose fatherly approach was a trait sorely missed by the son estranged from his true father. In fact, it was not until after his abduction during the Borg crisis that Picard ventured home, the first time in 20 years, and began to heal the rift with his brother Robert, who had been jealous of his high-achieving younger brother whom he viewed as getting away with spurning family traditions and responsibilities.
        
        In an early highlight of his illustrious and fondly recalled years aboard the U.S.S. Stargazer, First Officer Picard took command of the bridge upon his captain's death and saved the ship, leading to his permanent promotion to captain. His command has abruptly halted in 2355 when the vessel was abandoned with relatively little loss of life during an encounter that, years later, was realized to be the first UFP-Ferengi contact; casualties would have been much higher had he not devised a deceptive warp-speed jump maneuver that today is still studied and bears his name. Even so, he endured a standard inquiry a year later but was cleared of all negligence.
        
        It was only a year before the Stargazer's loss, in 2354, that Jack Crusher was killed on an away team, and he had returned the body to his widow at Starbase 32. That same year he visited Chalna; earlier, the Stargazer had barely eluded ambush while on an unsuccessful truce mission during the Cardassian border wars.
        
        Picard assumed captaincy of the U.S.S. Enterprise NCC-1701-D on SD 41124, having hand-picked much of his senior staff — such as two young officers who impressed him enough upon first meeting to win a place in the senior staff: Geordi La Forge once piloted his inspection tour shuttle and stayed up all night to refit an engine part he'd made a passing comment on, and he witnessed Tasha Yar risk her life to save colonists amid a Carnelian mine field. Finally, he had picked Riker from among simple resumes as his first officer and promoted him to commander sight unseen, impressed by his record of independence. His command presence and ethics persevered even through the Satarran memory wipe—despite orders, he would not fire on unarmed people.
        
        Within months of his Enterprise captaincy he was offered admiral's rank and the job of commandant of Starfleet Academy by Admiral Quinn but turned it down to retain his flagship. He also commanded the 23-ship blockade fleet to deter Romulan interference along the Klingon border during the empire's civil war of 2367-68, and undertook a covert raid in 2369 with two Enterprise officers on Celtris III to investigate a reported Cardassian metagenic weapons base, later found to be a hoax.
        
        Following the loss of the Enterprise at Veridian III, Picard won command of the ship's next namesake, one of the new Sovereign class, in 2372 on SD 49827.5. While Lt. Cmdr. Worf chose to be the exception, Picard's entire senior staff and many junior officers made the transfer with their captain. That continuity proves fortuitous: less than a year later, Picard was ordered away from repelling a second Borg attack for fear of giving unwitting aid to the enemy, but after reconsidering he led a deflection of the main assault. From there, a risky time-travel gambit paid off to correct temporal sabotage involving human first contact.
        
        Other mission performance highlights of his years on the Enterprises included his second meeting with Sarek, where at great personal risk he agreed to a mind-meld to save theLegaran conference in 2366 with the ailing ambassador; the legendary Vulcan had taken an interest in his career, calling it "satisfactory," but Picard was still awed by the UFP legend. They met again briefly as Sarek lay dying two years later as Picard was en route to another reunion with Spock, leading an underground pro-unification movement with Vulcan onRomulus.
        
        Picard has also participated in first-contact encounters with the Borg, Ferengi, Edo, Aldeans,Tamarians, Jarada, Malcorians, Douwd, Mintakans, Paxans, Cytherians, the Ux-Mal, andDevidians, among others, and served as a negotiator and diplomat on missions includingAcamar III, Rutia IV, Angosia III, Bajor, Talarians, Turkana IV, Pentaurus V, Ventax II,Kaelon II, Lenaria, Gemaris V, Dachlyd, and Krios-Valt Minor.
        
        Picard keeps a healthy outlook on life with a wide variety of interests and recreational pursuits, including his near-professional pursuit of archeology, having studied the Iconian culture since his cadet days and addressed the Federation Archeological Council as keynote speaker on his oft-studied Tagus III ruins in 2367. He enjoys Terran literature in its written rather than holo-visual display, especially detective fiction such as Dixon Hill, and Shakespearean drama; oddly enough, while he enjoys role-playing the former in holo-programs, he avoids acting or any other performance art himself despite an interest in classical music and attending the shipboard concerts and plays on the Enterprise. Even so, he overcame his childhood dislike and began playing a Kataanan flute following his encounter with that culture.
        
        Picard's interests go well beyond archeology and literature, however. The subject of planetary motion and physics is another; he kept up with the Atlantis Project on Earth through journals; and is fascinated to be the first to discover the spacefaring life form, communicate with the Crystalline Entity, and reveal an ancient Promellian battle cruiser. He has studied semantics and keeps his Latin fresh, but has no interest in politics, dance, small animals, or the Enterprise senior staff's poker game until his 2370 encounter with Q and an alternate future timeline.
        
        `,
        urls: [
          'https://en.wikipedia.org/wiki/Jean-Luc_Picard',
          'http://www.startrek.com/database_article/picard-jean-luc',
        ],
        img: picardImage,
        avatar: picardAvatar,
      },
      {
        id: 45,
        name: 'Worf',
        fullName: 'Worf, son of Mogh',
        race: 'klingon',
        biografy: `As the only Klingon in Starfleet, Worf has already achieved an illustrious and honorable career aboard the U.S.S. Enterprise as well as played a key role in Empire politics, but he keenly feels the effects of an often tragic life caught uniquely between the two conflicting cultures — immediately evidenced by the traditional Klingon baldrics he wears over his Starfleet uniform. This inner-felt conflict stems in part from his perception of honor as taught but not always practiced by his native people, and is complicated by family relationships which echo his duality of culture in both his personal and public life. Worf has even been put on report.

        He was born into a powerful political house on Qo'noS and carries vivid memories of a typical Klingon childhood. On his first ritual hunt before the age of six with his father's friend L'Kor, he attacked a large beast and it mauled his arm, providing a lifelong scar.
        
        However, Worf's life was changed forever in 2346 when his family was wiped out by Romulans at the Khitomer Outpost along their border; he has no memory of his father. The young man was thought to be the only survivor, and was soon adopted by Chief Sergey Rozhenko, a human engineer nearing retirement aboard the U.S.S. Intrepid, which provided the first assistance at the scene.
        
        The next year Worf lived with him, his wife Helena, and their son Nikolai among 20,000 colonists on the farm world Gault and later Earth, where the bigger and stronger Worf had a hard time adjusting to less-violent human culture and the two boys often disagreed. Finally, at the age of 13 while playing in a championship game as captain of his school soccer team, he unintentionally broke the neck of an opponent and the boy died a day later — forever guilting him into a life of restraint among humans. On the other hand, the Khitomer incident instilled in him a life-long hatred of Romulans.
        
        To feed his thirst for his native people's culture, the Rozhenkos consciously exposed Worf to as much as they possibly could — serving him Klingon food, including his favorite rokeg blood pie, and sending him to Qo'noS for his initial Age of Ascension ceremony in 2355, at age 15. As usual, when on the homeworld he stayed with a cousins' family but felt rejected and ran away to the nearby mountains. There, while undergoing the Rite of MajQua in the lava caves of No'Mat, the vision of the original Klingon warrior Kahless came to him, prophesying that Worf would do what no other Klingon had done.
        
        Worf entered Starfleet Academy with Nikolai in 2357, but his impetuous brother left school and returned to Gault while Worf went on to graduate in 2361. The fear of depending on others to protect him had been the prime point of his own entrance exam's psych test.
        
        In 2364 he signed aboard Picard's U.S.S. Enterprise in command division as a junior-grade lieutenant, at the time wearing a century-old Klingon baldric. After the death of Security Chief Tasha Yar, he became acting chief and then assumed the post full-time in early 2365, switching to security full-time n the operations division and gaining a promotion to full lieutenant. His shipmates formally promoted him to lieutenant commander six years later with a ceremonial holographic ocean-dunking on an ancient Earth naval vessel.
        
        Aside from a few weeks of dating fellow officer Deanna Troi in 2370 on the U.S.S. Enterprise, his most serious romance to date involved the half-human Ambassador K'Ehleyr. Worf had ended their initial affair in 2359, during his Academy years, but K'Ehleyr refused to begin anew and take vows after they mated in 2365 during her mission regarding the T'Ong sleeper ship incident.
        
        Worf's family tree took on surprising twists during his U.S.S. Enterprise career, beginning with the trumped-up charge that Mogh had betrayed Khitomer to the Romulans. The resulting probe turned up not only a second survivor and eyewitness to the massacre, his old nursemaid, but a younger brother who'd been left behind on Qo'noS, Kurn. Even when the traitor was proven to be not Mogh but Jared, father of the powerful Duras, Worf later accepted discommendation from Klingon society rather than cause an uproar in Empire politics had the cover-up been revealed.
        
        Worf was shocked to discover in 2367 that his interludes with K'Ehleyr had fostered a son, Alexander, when she accompanied the dying Klingon Chancellor K'mpec while old foe Duras, a challenger for succession, was a suspect. With her mate and son present, K'Ehleyr died after being attacked by Duras when she drew too close to the truth about Khitomer, and Worf in anguish killed Duras on his own ship. His captain was more than understanding, as he had been when Worf refused to donate blood to save a Romulan, but he was put on formal report for his actions.
        
        During the Klingon Civil War of 2367-68 Worf felt compelled to resign his Starfleet commission to become involved, but it was reactivated after the war. During that time he persuaded Kurn to support Gowron against Duras' sisters and their Romulan backers, standing up to the sisters when abducted and tortured. His aid of the victor Gowron eventually restored his family's honor, and Kurn won a seat on the High Council.
        
        Mogh was later rumored to be alive in a secret Romulan prison on Carraya IV, but though Worf's covert 2369 mission found the rumor to indeed be false he did discover — and agree to keep secret — a colony of shamed Klingon survivors from Khitomer, led by his father's old friend, L'Kor, and their Romulans guards who'd resigned to live with them.
        
        Worf dipped back into Klingon politics in 2370 after he questioned his own faith in the teaching of Kahless following the Carraya IV incident. His visit to the caves of Boreth, the legendary site of the great warrior's predicted return, was shaken up when Kahless did appear to return. Although later found to be cloned from ancient relics of the original Klingon warrior by the Boreth clerics, the response of spiritually empty Klingons to his presence led Worf to insist that Gowron accept the cloned Kahless as a returned Emperor and moral leader — in effect creating a constitutional theo-monarchy.
        
        He was even reunited with his foster brother Nikolai in 2370, when the two clashed again over the human's saving of the doomed Borallan village against Picard's orders and the Prime Directive to save his pregnant mate, a native. The two parted more amicably after the incident, however.
        
        After his mother's death Alexander was initially sent to live with the Rozhenkos on Earth, but a year later Helena returned with him to plead that Worf take him back for support and guidance. The two shared a testy relationship at first, but thanks to sessions with the ship's counselor — whom he eventually selected as the boy's foster parent if need be — they fared better. When a shipboard accident left him paralyzed, Worf considered the ritual Hegh'bat suicide until both Riker and Troi talked him out of it, pointing to Alexander's need for a parent; an experimental genotronic spine later restored his health. Shocked in 2370 to find his son returned through a time loop from 40 years in the future, be began allowing Alexander to find his own way — even if it was not the way of a Klingon warrior.
        
        During his U.S.S. Enterprise tenure, he birthed Keiko O'Brien's baby Molly in Ten-Forward during a shipwide crisis in 2368, his only prior experience having been a Starfleet emergency first aid class. He dislikes surprise parties and diplomatic duty.
        
        He also taught mok'bara classes to those interested aboard ship, won a bat'leth tournament on Forkas III in 2370, and for a time tutored Dr. Crusher on the weapon; there is no word that he took her offer to join her acting workshop. He trains with a multi-level holo-program of personal combat "calisthenics," has also played Parrises Squares, and picked up the nickname "Iceman" from his U.S.S. Enterprise poker play. Other interests include Klingon novels, love poetry, and a love of Klingon opera. His favorite beverage, christened as a "warrior's drink" when introduced to it by Guinan, is prune juice.
        
        Following the destruction of the Enterprise and break-up of its staff in 2371, Worf sent Alexander once again to live with the Rozhenkos on Earth and went on extended leave to revisit the Klingon monastery and clerics of Boreth in search of a spiritual answer to the letdown the rapid events provoked. He found their discussions enlightening and considered resigning his Starfleet commission, but in early 2372 he accepted Captain Benjamin Sisko's request to join the Deep Space Nine staff in light of renewed Klingon friction after dissolution of the Khitomer Accords and their short-lived invasion of the Cardassian Empire. He had all but decided to resign and join a Nyberrite cruiser crew when the Deep Space Nine offer persuaded him to stay, having felt that his Starfleet uniform was a disgrace to his own people.
        
        Early on in the assignment Worf admitted to continued bouts of depression over the end of what he perceived as glory days on the Enterprise, and countered it somewhat by taking quarters on the station's starship, the U.S.S. Defiant, and finding a kinship with Dax, who trains with the bat'leth and mek'leth as well.
        
        He soon got the chance to meet Klingon legend Kor, but that honor too was ripped away when image gave way to reality as the two fought over the Sword of Kahless relic they found on a quest.
        
        Worf's public opposition to Gowron's invasion left him largely unaffected until the Empire attempted to frame him for the so-called slaughter of 141 Klingon civilians amid a skirmish; the hoax was revealed only shortly before he would have been extradited for the crime and faced certain death. However, on Qo'noS his house was once again stripped of its honor and properties, including Kurn's seat on the High Council.
        
        His depressed brother showed up on the station asking for his own suicide rite. Only Dax's interruption stopped the ritual Worf was aiding, but after Kurn's unsuccessful death wish as a Bajoran deputy Worf realized his brother had no future and, short of suicide, opted to have his memory wiped and replaced with another Klingon identity, sending him to live with a family friend. Even then he lived with the regret that his actions had been forever tainted by his human-learned values of mercy.`,
        urls: [
          'https://en.wikipedia.org/wiki/Worf',
          'http://www.startrek.com/database_article/worf',
        ],
        img: worfImage,
        avatar: worfAvatar,
      },
    ],
  },
}));

export default characterAPI;
