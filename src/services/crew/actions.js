import {
  LOAD_CREW,
  ADD_MEMBER,
  REMOVE_MEMBER,
} from './actionTypes';

export const loadCrew = members => ({
  type: LOAD_CREW,
  payload: members,
});

export const addMember = member => ({
  type: ADD_MEMBER,
  payload: { ...member },
});

export const removeMember = id => ({
  type: REMOVE_MEMBER,
  payload: { id },
});
