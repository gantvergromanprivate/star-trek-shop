import * as actions from '../actions';
import * as types from '../actionTypes';

const mockMember = {
  id: 42,
  name: 'Kirk, James T.',
  fullName: 'James Tiberius Kirk',
  race: 'human',
  urls: [
    'https://en.wikipedia.org/wiki/James_T._Kirk',
    'http://www.startrek.com/database_article/james-t-kirk',
  ],
};

describe('floatCrew actions', () => {
  describe('loadCrew', () => {
    it('should return expected payload', () => {
      const expectedAction = {
        type: types.LOAD_CREW,
        payload: mockMember,
      };

      expect(actions.loadCrew(mockMember)).toEqual(expectedAction);
    });
  });

  describe('addMember', () => {
    it('should return expected payload', () => {
      const expectedAction = {
        type: types.ADD_MEMBER,
        payload: mockMember,
      };

      expect(actions.addMember(mockMember)).toEqual(expectedAction);
    });
  });

  describe('removeMember', () => {
    it('should return expected payload', () => {
      const expectedAction = {
        type: types.REMOVE_MEMBER,
        payload: mockMember,
      };

      expect(actions.removeMember(mockMember)).toEqual(expectedAction);
    });
  });
});
