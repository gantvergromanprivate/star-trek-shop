import {
  LOAD_CREW,
  ADD_MEMBER,
  REMOVE_MEMBER,
} from './actionTypes';

const initialState = {
  members: [],
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case LOAD_CREW:
      return {
        ...state,
        members: payload,
      };
    case ADD_MEMBER:
      if (!state.members.some(({ id }) => payload.id === id)) {
        return {
          ...state,
          members: [...state.members, payload],
        };
      }
      return state;
    case REMOVE_MEMBER:
      return {
        ...state,
        members: state.members.filter(({ id }) => id !== payload.id),
      };
    default:
      return state;
  }
};
