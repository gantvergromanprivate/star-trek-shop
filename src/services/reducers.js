import { combineReducers } from 'redux';
import crewReducer from './crew/reducer';
import shelfReducer from './shelf/reducer';
import filtersReducer from './filters/reducer';
import sortReducer from './sort/reducer';

export default combineReducers({
  crew: crewReducer,
  shelf: shelfReducer,
  filters: filtersReducer,
  sort: sortReducer,
});
