import { UPDATE_SORT } from './actionTypes';

const updateSort = (sort => ({
  type: UPDATE_SORT,
  payload: sort,
}));

export default updateSort;
