import UPDATE_FILTER from './actionTypes';

export default filters => ({
  type: UPDATE_FILTER,
  payload: filters,
});
