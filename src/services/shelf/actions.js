import FETCH_CHARACTERS from './actionTypes';

import characterAPI from '../../utils';

const compare = {
  // TODO: add more flexible logic here
  ascNames: (a, b) => {
    if (a.name < b.name) return -1;
    if (a.name > b.name) return 1;
    return 0;
  },
  descNames: (a, b) => {
    if (a.name > b.name) return -1;
    if (a.name < b.name) return 1;
    return 0;
  },
};

export default (filters, sortBy, callback) => dispatch => (
  // TODO: remove stub and use real fetch or axios
  characterAPI()
    .then((res) => {
      let { characters } = res.data;

      if (filters) {
        characters = characters.filter(
          ({ name }) => name.toUpperCase().includes(filters.toUpperCase()),
        );
      }

      if (sortBy) {
        characters = characters.sort(compare[sortBy]);
      }

      if (callback) {
        callback();
      }

      return dispatch({
        type: FETCH_CHARACTERS,
        payload: characters,
      });
    })
    .catch(() => {
      // eslint-disable-next-line no-console
      console.log('Could not fetch characters. Try again later.');
    })
);
