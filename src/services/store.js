import { compose, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';

export default (initialState) => {
  const stateParam = JSON.parse(window.localStorage.getItem('state')) || initialState;
  const middleware = [thunk];

  // eslint-disable-next-line no-underscore-dangle
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const store = createStore(
    rootReducer,
    stateParam,
    composeEnhancers(
      applyMiddleware(...middleware),
    ),
  );

  store.subscribe(() => {
    const state = store.getState();
    const persist = {
      crew: state.crew,
    };

    window.localStorage.setItem('state', JSON.stringify(persist));
  });

  return store;
};
